import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { MainConfigService } from '../main-config.service';

@Injectable({
  providedIn: 'root'
})
export class EmployeeDataService {

  constructor(private http: HttpClient, private mainconfig: MainConfigService) {}

  addEmployee(data: any): Observable<any> {
    return this.http.post(this.mainconfig.API_URL + '/employee', data);
  }

  updateEmployee(id: number, data: any): Observable<any> {
    return this.http.put( this.mainconfig.API_URL + `/employee/${id}`, data);
  }

  getEmployeeList(): Observable<any> {
    return this.http.get(this.mainconfig.API_URL + '/employee');
  }

  deleteEmployee(id: number): Observable<any> {
    return this.http.delete(this.mainconfig.API_URL + `/employee/${id}`);
  }

}
