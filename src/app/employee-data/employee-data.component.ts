import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { EmployeeDataService } from './employee-data.service';
import Swal from 'sweetalert2';
import { MatPaginator } from '@angular/material/paginator';
import { MatDialog } from '@angular/material/dialog';
import { EmployeeDataDetailComponent } from './employee-data-detail/employee-data-detail.component';

@Component({
  selector: 'app-employee-data',
  templateUrl: './employee-data.component.html',
  styleUrls: ['./employee-data.component.css']
})
export class EmployeeDataComponent implements OnInit, AfterViewInit {
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  constructor(
    private employeeservice: EmployeeDataService,
    private dialog: MatDialog,
  ) {}

  addEmployee = new FormGroup({
    firstName : new FormControl('', Validators.required),
    lastName : new FormControl('', Validators.required),
    email : new FormControl('', [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]),
    birthDate : new FormControl('', Validators.required),
    basicSalary : new FormControl(0, Validators.required),
    status : new FormControl('1'),
    group : new FormControl('', Validators.required),
    description : new FormControl(new Date())
  })

  allData = []
  tabIndex = 0;
  addMode = false

  groupList = [
    {
      name : 'Project Development'
    },
    {
      name : 'Product Development'
    },
    {
      name : 'Accounting'
    },
    {
      name : 'Brand Management'
    },
    {
      name : 'Customer Relations'
    },
    {
      name : 'Human Resources'
    },
    {
      name : 'Payroll'
    },
    {
      name : 'Research & Development'
    },
    {
      name : 'Sales'
    },
    {
      name : 'Marketing'
    }
  ]

  groupListFilter = [
    {
      name : 'All',
      value : ''
    },
    {
      name : 'Product Development',
      value : 'Product Development'
    },
    {
      name : 'Accounting',
      value : 'Accounting'
    },
    {
      name : 'Brand Management',
      value : 'Brand Management',
    },
    {
      name : 'Customer Relations',
      value : 'Customer Relations'
    },
    {
      name : 'Human Resources',
      value : 'Human Resources'
    },
    {
      name : 'Payroll',
      value : 'Payroll'
    },
    {
      name : 'Research & Development',
      value : 'Research & Development'
    },
    {
      name : 'Sales',
      value : 'Sales'
    },
    {
      name : 'Marketing',
      value : 'Marketing'
    }
  ]

  isDisabled = true
  maxDate = new Date()
  employeeData = new MatTableDataSource([])
  employeeDataColumn = [
    'firstName',
    'lastName',
    'group',
    'status',
    'action'
  ]

  paginatorEmployee = {
    total: 0,
    skip: 0,
    limit: 10,
  };

  sizeOptions = [10, 20, 50, 100];

  firstName_search = ''
  group_search = new FormControl('')

  getEmployeeList() {
    this.employeeservice.getEmployeeList().subscribe((response) => {
      console.log(response, "response");
      if(response.length > 0) {
        this.employeeData.data = response
        this.employeeData.filter = ''
        this.allData = JSON.parse(JSON.stringify(response))
      } else {
        Swal.fire({
          title: "Data Empty, Please Create Employee",
          icon: "error"
        });
      }

    })
  }

  applyFilter() {
    let filteredTable = []
    let firstName = this.firstName_search
    let groupName = this.group_search.value

    console.log(this.group_search.value, "filter group value");
    

    filteredTable= this.allData.filter((employee : any) =>
      (!firstName || employee['firstName'].toLowerCase().includes(firstName.toLowerCase())) &&
      (!groupName || employee['group'].toLowerCase().includes(groupName.toLowerCase()))
    );

    console.log(filteredTable, "filteredTable");
    
    this.employeeData.data = filteredTable
    this.employeeData.filter = ''
    this.employeeData.paginator = this.paginator

  }

  createEmployee(){
    const dataInject = {
      createMode: true,
    };
  const dialogRef = this.dialog.open(EmployeeDataDetailComponent, {
      data: dataInject,
      minHeight: '90%',
      minWidth: '70%',
      maxWidth: '80%',
      disableClose: true,
      panelClass: 'icon-outside',
  });
  dialogRef.afterClosed().subscribe((response) => {
      if(response.statusCode === 1) {
        if(this.firstName_search === '' && this.group_search.value === ''){
          this.getEmployeeList()
        } else {
          this.applyFilter()
        }
      }
  });
  }

  async deleteEmployee(id:any) {

    const result = await Swal.fire({
      title: "Are you sure?",
      icon: "question",
      showCancelButton: true
    });

    console.log(result, "result");

    if (!result.isConfirmed) {
        return;
    }

    this.employeeservice.deleteEmployee(id).subscribe({
      next: (res) => {
        this.getEmployeeList();
        Swal.fire({
          title: "Data success Deleted",
          icon: "success"
        });
      },
      error: console.log,
    });
  }

  editEmployee(element: any) {
    const dataInject = {
        editMode: true,
        ...element
      };
    const dialogRef = this.dialog.open(EmployeeDataDetailComponent, {
        data: dataInject,
        minHeight: '90%',
        minWidth: '70%',
        maxWidth: '80%',
        disableClose: true,
        panelClass: 'icon-outside',
    });
    dialogRef.afterClosed().subscribe((response) => {
        if(response.statusCode === 1) {
          if(this.firstName_search === '' && this.group_search.value === ''){
            this.getEmployeeList()
          } else {
            this.applyFilter()
          }
        }
    });
  }

  detailEmployee(element: any) {
    const dataInject = {
        editMode: false,
        ...element
      };
    const dialogRef = this.dialog.open(EmployeeDataDetailComponent, {
        data: dataInject,
        minHeight: '90%',
        minWidth: '70%',
        maxWidth: '80%',
        disableClose: true,
        panelClass: 'icon-outside',
    });
    dialogRef.afterClosed().subscribe((response) => {
        if(response.statusCode === 1) {
          if(this.firstName_search === '' && this.group_search.value === ''){
            this.getEmployeeList()
          } else {
            this.applyFilter()
          }
        }
    });
  }

  ngOnInit(): void {
    this.getEmployeeList();
  }

  ngAfterViewInit() {
    this.employeeData.paginator = this.paginator;
  }
  
}
