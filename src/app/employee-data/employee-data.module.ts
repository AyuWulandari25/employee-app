import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmployeeDataRoutingModule } from './employee-data-routing.module';
import { EmployeeDataComponent } from './employee-data.component';
import { MaterialModule } from '../material-module';
import { EmployeeDataDetailComponent } from './employee-data-detail/employee-data-detail.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    EmployeeDataComponent,
    EmployeeDataDetailComponent
  ],
  imports: [
    CommonModule,
    EmployeeDataRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class EmployeeDataModule { }
