import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeDataDetailComponent } from './employee-data-detail.component';

describe('EmployeeDataDetailComponent', () => {
  let component: EmployeeDataDetailComponent;
  let fixture: ComponentFixture<EmployeeDataDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmployeeDataDetailComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EmployeeDataDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
