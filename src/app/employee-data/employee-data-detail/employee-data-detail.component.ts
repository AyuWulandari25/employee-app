import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import Swal from 'sweetalert2';
import { EmployeeDataService } from '../employee-data.service';

@Component({
  selector: 'app-employee-data-detail',
  templateUrl: './employee-data-detail.component.html',
  styleUrls: ['./employee-data-detail.component.css']
})
export class EmployeeDataDetailComponent implements OnInit{
  constructor(
    private dialogRef: MatDialogRef<EmployeeDataDetailComponent>,
    public dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private employeedataservice : EmployeeDataService
  ) {}

  cancelDialog() {
    this.dialogRef.close({ statusCode: 0 });
    if(!this.data['createdMode']){
      this.editEmployee.reset()
    } else {
      this.addEmployee.reset()
    }
  }

  editEmployee = new FormGroup({
    firstName : new FormControl('', Validators.required),
    lastName : new FormControl('', Validators.required),
    email : new FormControl('', [Validators.required, Validators.email]),
    birthDate : new FormControl('', Validators.required),
    basicSalary : new FormControl(0, Validators.required),
    status : new FormControl('1'),
    group : new FormControl('', Validators.required),
    description : new FormControl(new Date())
  })

  addEmployee = new FormGroup({
    firstName : new FormControl('', Validators.required),
    lastName : new FormControl('', Validators.required),
    email : new FormControl('', [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]),
    birthDate : new FormControl('', Validators.required),
    basicSalary : new FormControl(0, Validators.required),
    status : new FormControl('1'),
    group : new FormControl('', Validators.required),
    description : new FormControl(new Date())
  })

  maxDate = new Date()

  groupList = [
    {
      name : 'Project Development'
    },
    {
      name : 'Product Development'
    },
    {
      name : 'Accounting'
    },
    {
      name : 'Brand Management'
    },
    {
      name : 'Customer Relations'
    },
    {
      name : 'Human Resources'
    },
    {
      name : 'Payroll'
    },
    {
      name : 'Research & Development'
    },
    {
      name : 'Sales'
    },
    {
      name : 'Marketing'
    }
  ]

  async editDataEmployee(){
    console.log(this.editEmployee.value);
    
    const result = await Swal.fire({
      title: "Are you sure?",
      icon: "question",
      showCancelButton: true
    });

    console.log(result, "result");

    if (!result.isConfirmed) {
        return;
    }

    this.employeedataservice.updateEmployee(this.data.id, this.editEmployee.value)
          .subscribe({
            next: (val: any) => {
              Swal.fire({
                title: "Data success Updated",
                icon: "success"
              });
              this.dialogRef.close({statusCode: 1});
            },
            error: (err: any) => {
              console.error(err);
            },
          });
  }

  async createEmployeeData(){
    
    if(this.addEmployee.invalid){
      Swal.fire({
        title: "Data harus terisi dengan benar",
        icon: "error"
      });
      
      return
    }
    
    console.log(this.addEmployee.value);
    const result = await Swal.fire({
      title: "Are you sure?",
      icon: "question",
      showCancelButton: true
    });

    console.log(result, "result");

    if (!result.isConfirmed) {
        return;
    }

    this.employeedataservice.addEmployee(this.addEmployee.value).subscribe({
      next: (val: any) => {
        this.dialogRef.close({statusCode: 1});
        Swal.fire({
          title: "Data success Created",
          icon: "success"
        });
      },
      error: (err: any) => {
        Swal.fire({
          title: err,
          icon: "error"
        });
      },
    });
  }

  ngOnInit(): void {
    console.log("data", this.data);
    if(this.data){
      this.editEmployee.controls.basicSalary.setValue(parseInt(this.data.basicSalary))
      this.editEmployee.controls.firstName.setValue(this.data.firstName)
      this.editEmployee.controls.birthDate.setValue(this.data.birthDate)
      this.editEmployee.controls.email.setValue(this.data.email)
      this.editEmployee.controls.group.setValue(this.data.group)
      this.editEmployee.controls.lastName.setValue(this.data.lastName)
    }
  }
}
