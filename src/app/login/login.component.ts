import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  constructor(
    public router: Router,
  ) {}
  hide = true;
  userLogin = new FormGroup({
    username : new FormControl(''),
    password : new FormControl('')
  })

  userLoginTemplate = {
    username : 'admin',
    password : 'admin123'
  }

  async login(){
    if(this.userLogin.controls.username.value === '' && this.userLogin.controls.password.value === ''){
      Swal.fire({
        title: "Data Harus Terisi Semua",
        icon: "error"
      });

      return
    }

    if(this.userLogin.controls.password.value === ''){
      Swal.fire({
        title: "Password harus diisi",
        icon: "error"
      });

      return
    }

    if(this.userLogin.controls.username.value === ''){
      Swal.fire({
        title: "Username harus diisi",
        icon: "error"
      });

      return
    }

    if(this.userLogin.controls.username.value !== this.userLoginTemplate.username){
      Swal.fire({
        title: "Username salah, silahkan input dengan benar",
        icon: "error"
      });

      return
    }

    if(this.userLogin.controls.password.value !== this.userLoginTemplate.password){
      Swal.fire({
        title: "Password salah, silahkan input dengan benar",
        icon: "error"
      });

      return
    }

    this.router.navigate(['/employee-data'])
  }
}
